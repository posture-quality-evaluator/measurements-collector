from flask import Flask, escape, request
import json


app = Flask(__name__)

@app.route('/pose', methods=['POST'])
def hello():
    data = request.get_json(force=True)
    save_new_measurements(data['measurements'])
    return 'Success'


def save_new_measurements(new_poses):
    data = load_data()
    data['measurements'] = data['measurements'] + new_poses
    save_data(data)


def load_data(filename='measurements.json'):
    try:
        with open(filename, 'r') as f:
            return json.load(f)
    except FileNotFoundError:
        app.logger.warn("Couldn't read from file %s", filename)
        return {'measurements': []}


def save_data(data, filename='measurements.json'):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5050)
