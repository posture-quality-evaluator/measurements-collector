# Metrics Collector 🗑

This is Python Flask service which can only receive incoming POST requiests at ```/pose``` with body containing new measurement and append it in local ```measurements.json``` file.

## Installation

### Unix

```shell
$ make install
```

### Windows

Download <a href="https://ubuntu.com/download/desktop">Ubuntu</a>. Then see Unix installation.

Or if you are a profi Windows user and you know how to deal with `virtualenv` you can try creating bat makefile or any other helper files needed for your OS to run the code.

## Usage

This will run service on 0.0.0.0:5050.

### Unix

```shell
$ make run
```
